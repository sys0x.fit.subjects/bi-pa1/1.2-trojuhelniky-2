#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define DBL_EPSILON 2.2204460492503131e-16 // double
int invalidInput(void){
    printf("Nespravny vstup.\n");
    //return 1;
    exit(1);
}
int invalidTroj(void){
    printf("Neplatny trojuhelnik.\n");
    //return 1;
    exit(1);
}

int checkType(){
    char char1,char2;
    scanf(" %c %c",&char1,&char2);
   if (char1!='{'){invalidInput();}
    if(char2=='['){
        return 6;
    }else{
        ungetc(char2,stdin);

        return 3;
    }

}
int get6Numbers(double *bodXA, double *bodXB, double *bodXC, double *bodYA, double *bodYB, double *bodYC){
    char slozenaZavorka;
    int check;
    check = scanf(" %lf ; %lf ] , [ %lf ; %lf ] , [ %lf ; %lf ] %c",&*bodXA,&*bodYA,&*bodXB,&*bodYB,&*bodXC,&*bodYC,&slozenaZavorka);

    if(check !=7||slozenaZavorka!='}'){
        invalidInput();
    }
return 0;

}
int get3Numbers(double *sideAB,double *sideAC,double *sideBC){
    char slozenaZavorka2;
    int check;
    check = scanf(" %lf , %lf , %lf %c",&*sideAB,&*sideAC,&*sideBC,&slozenaZavorka2);

    if(check !=4||slozenaZavorka2!='}'){
        invalidInput();
    }
    return 0;

}


int getAngel(double sideAB,double sideAC,double sideBC,double *alfa,double *beta,double *gamma){
    *alfa=(pow(sideBC, 2)-pow(sideAC, 2)-pow(sideAC, 2))/(-2*sideAC*sideAB);
    *beta=(pow(sideAC, 2)-pow(sideAC, 2)-pow(sideBC, 2))/(-2*sideAB*sideBC);
    *gamma=(pow(sideAB, 2)-pow(sideAC, 2)-pow(sideBC, 2))/(-2*sideBC*sideAC);

    return 0;
}
int sortEdges(double * sideAB,double * sideAC,double * sideBC){
    double temp;
    if(* sideAB<* sideAC){
        temp= * sideAC;
        * sideAC=* sideAB;
        * sideAB=temp;
    }
    if(* sideAB<* sideBC){
        temp= * sideBC;
        * sideBC=* sideAB;
        * sideAB=temp;
    }
    if(* sideAC<* sideBC){
        temp= * sideBC;
        * sideBC=* sideAC;
        * sideAC=temp;
    }
    return 0;
}
int createEdges(double * bodXA,double * bodXB,double * bodXC,double * bodYA,double * bodYB,double * bodYC,double * sideAB,double * sideAC,double * sideBC ){
    * sideAB=sqrt(pow(* bodXA-* bodXB,2)+pow(* bodYA-* bodYB,2));
    * sideAC=sqrt(pow(* bodXA-* bodXC,2)+pow(* bodYA-* bodYC,2));
    * sideBC=sqrt(pow(* bodXB-* bodXC,2)+pow(* bodYB-* bodYC,2));
    return 0;
}
int checkIfTriangle(double sideAC, double sideAB, double sideBC){
    double alfa, beta, gamma;
    getAngel(sideAC,sideAB,sideBC,&alfa,&beta,&gamma);
    double DBL_EPS1000=1000*DBL_EPSILON;
    if(fabs(fabs(alfa)-1)<=DBL_EPS1000||fabs((beta)-1)<=DBL_EPS1000||fabs(fabs(gamma)-1)<=DBL_EPS1000||sideAB==0||sideAC==0||sideBC==0||sideAB<0||sideAC<0||sideBC<0||sideAB>(sideAC+sideBC)){
        invalidTroj();
        return 1;
    }
    return 0;
}
int generateOutput(double sideAB1,double sideAC1,double sideBC1,double sideAB2,double sideAC2,double sideBC2){
    if((fabs(sideAB1-sideAB2)+fabs(sideAC1-sideAC2)+fabs(sideBC1-sideBC2))<=DBL_EPSILON*10000*(fabs(sideAB1+sideAB2)+fabs(sideAC1+sideAC2)+fabs(sideBC1+sideBC2))){
        printf("Trojuhelniky jsou shodne.\n");
        return 0;
    }
    double obvod1=(sideAB1+sideAC1+sideBC1);
    double obvod2=(sideAB2+sideAC2+sideBC2);

    if(fabs(obvod1-obvod2)<=DBL_EPSILON*10000*fabs(obvod1+obvod2)){
        printf("Trojuhelniky nejsou shodne, ale maji stejny obvod.\n");

        return 0;
    }else if(obvod1<obvod2){
        printf("Trojuhelnik #2 ma vetsi obvod.\n");

        return 0;
    }
    printf("Trojuhelnik #1 ma vetsi obvod.\n");
    return 0;
}
int main(void) {
    double bodXA1,bodXB1,bodXC1;
    double bodYA1,bodYB1,bodYC1;

    double bodXA2,bodXB2,bodXC2;
    double bodYA2,bodYB2,bodYC2;

    double sideAB1,sideAC1,sideBC1,sideAB2,sideAC2,sideBC2;

    printf("Trojuhelnik #1:\n");
    if(checkType()==6){
        get6Numbers(&bodXA1, &bodXB1, &bodXC1, &bodYA1, &bodYB1, &bodYC1);
        createEdges(&bodXA1,&bodXB1,&bodXC1,&bodYA1,&bodYB1,&bodYC1,&sideAB1,&sideAC1,&sideBC1);
        sortEdges(&sideAB1,&sideAC1,&sideBC1);
        if(checkIfTriangle(sideAB1, sideAC1, sideBC1)==1){ return 1; }

    }else{
        
        get3Numbers(&sideAB1,&sideAC1,&sideBC1);
        sortEdges(&sideAB1,&sideAC1,&sideBC1);
        checkIfTriangle(sideAB1, sideAC1, sideBC1);
    }

    printf("Trojuhelnik #2:\n");
    if(checkType()==6){
        get6Numbers(&bodXA2, &bodXB2, &bodXC2, &bodYA2, &bodYB2, &bodYC2);
        createEdges(&bodXA2,&bodXB2,&bodXC2,&bodYA2,&bodYB2,&bodYC2,&sideAB2,&sideAC2,&sideBC2);
        sortEdges(&sideAB2,&sideAC2,&sideBC2);
        if(checkIfTriangle(sideAB2, sideAC2, sideBC2)==1){ return 1; }

    }else{
        get3Numbers(&sideAB2,&sideAC2,&sideBC2);
        sortEdges(&sideAB2,&sideAC2,&sideBC2);
        checkIfTriangle(sideAB2, sideAC2, sideBC2);
    }

    generateOutput(sideAB1, sideAC1, sideBC1, sideAB2,sideAC2, sideBC2);
    return 0;
}